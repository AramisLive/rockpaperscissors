package ru.test;

public abstract class Player {
    public abstract Result getResult();
}
